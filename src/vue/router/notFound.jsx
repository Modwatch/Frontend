const component = {
  render(h) {
    return (
      <section>
        <h1>404 Not Found</h1>
        <p>Good try, but this page doesn't exist</p>
      </section>
    );
  }
};

export default component;
// export default function factory() {
//   return Promise.resolve(component);
// }
